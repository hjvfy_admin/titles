from django.contrib import admin
from django.contrib.auth.models import Group
from models import Site


@admin.register(Site)
class SiteAdmin(admin.ModelAdmin):
    list_display = ['url']

admin.site.unregister(Group)