# -*- coding: utf-8 -*-
from django.shortcuts import render
from django.http import HttpResponse, Http404
import json

try:
    from functools import wraps
except ImportError:
    def wraps(wrapped, assigned=('__module__', '__name__', '__doc__'),
              updated=('__dict__',)):
        def inner(wrapper):
            for attr in assigned:
                setattr(wrapper, attr, getattr(wrapped, attr))
            for attr in updated:
                getattr(wrapper, attr).update(getattr(wrapped, attr, {}))
            return wrapper
        return inner


def render_to(tmpl=None, content_type=None):
    def renderer(function):
        @wraps(function)
        def wrapper(request, *args, **kwargs):
            output = function(request, *args, **kwargs)
            if not isinstance(output, dict):
                return output
            return render(request, tmpl, output, content_type=content_type)
        return wrapper
    return renderer


class JsonResponse(HttpResponse):
    """
    HttpResponse descendant, which return response with ``application/json`` mimetype.
    """
    def __init__(self, data):
        super(JsonResponse, self).__init__(content=json.dumps(data), content_type='application/json')


def ajax_request(function):
    """
    If view returned serializable dict, returns JsonResponse with this dict as content.
    """
    @wraps(function)
    def wrapper(*args, **kwargs):
        if args[0].is_ajax():
            response = function(*args, **kwargs)
            if isinstance(response, dict) or isinstance(response, list):
                return JsonResponse(response)
            else:
                return response
        else:
            raise Http404()
    return wrapper