from decorators import render_to, ajax_request
from BeautifulSoup import BeautifulSoup
from models import Site
import urllib2
import json


@render_to('home.html')
def home(request):
    urls = list(Site.objects.values_list('url', flat=True))
    return {'urls': json.dumps(urls)}


@ajax_request
def fetch(request):
    url = request.GET.get('url')
    if not url.startswith('http://'):
        url = 'http://' + url if url.startswith('www.') else 'http://www.' + url
    content = urllib2.urlopen(url).read()
    soup = BeautifulSoup(content)
    title = soup.html.head.title.string
    return {'title': title}