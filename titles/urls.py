from django.conf.urls import patterns, include, url
from django.contrib import admin

urlpatterns = patterns(
    'titles.views',
    url(r'^$', 'home', name='home'),
    url(r'^fetch/$', 'fetch', name='fetch'),
    url(r'^admin/', include(admin.site.urls)),
)
